import Vue from "vue";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = false;

// https://bootstrap-vue.js.org/docs
import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
// End BootstrapVue

// https://github.com/KoRiGaN/Vue2Leaflet
import { Icon }  from 'leaflet';
import 'leaflet/dist/leaflet.css';

// this part resolve an issue where the markers would not appear
delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});
// End Vue2Leaflet

// https://www.npmjs.com/package/vue-moment
Vue.use(require('vue-moment'));

// Websockets
import ReconnectingWebSocket from 'reconnectingwebsocket'

let url = 'ws://'+window.location.hostname+':8000/ws/comm-issues/'
let ws = new ReconnectingWebSocket(url)

ws.onopen = function() {
  console.log('connection opened')
}
ws.onmessage = function(e){
  let data = JSON.parse(e.data)
  if (data.type == 'comm-issues') {
    store.commit('ADD_COMM_ISSUES', data.message)
  }
}



new Vue({
  store,
  render: h => h(App)
}).$mount("#app");