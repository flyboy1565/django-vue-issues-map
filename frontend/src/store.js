import Vue from "vue";
import Vuex from "vuex";
import { latLng } from "leaflet";

// https://stackoverflow.com/questions/48650107/use-axios-globally-in-all-my-components-vue
import axios from 'axios';
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {},
    markers: [],
    show_markers: [],
    new_stores: [
      {
        store:5999,
        days_till: 24,
        open_date: '2019-11-02',
        tickets: [
          {
            id:124322,
            title: "New Store Ticket"
          }
        ]
      }
    ]
  },
  getters: {
    logged_in: state => {
      return state.user.keys().length > 0
    },
    markers: state => {
      return state.markers
    },
    comm_down: state => {
      let d = state.markers.filter(f => f.issue === 'comm' )
      return d
    },
    as400_down: state => {
      let d = state.markers.filter(f => f.issue === 'as400' )
      return d
    },
    linux_down: state => {
      let d = state.markers.filter(f => f.issue === 'linux' )
      return d
    },
    power_outages: state => {
      let d = state.markers.filter(f => f.issue === 'power' )
      return d
    },
    unknown: state => {
      let d = state.markers.filter(f => f.issue === 'unknown' )
      return d
    },
    new_stores: state => {
      return state.new_stores
    }
  },
  mutations: {
    UPDATE_TICKET(state, payload) {
      let s = state.markers.filter(element => element.store = payload.store)
      s[0].ticket = payload.value
    }, 
    LOGIN(state, payload) {
      state.user.$set(payload)
    },
    ADD_COMM_ISSUES(state, issues){
      issues.forEach((issue) => {
        let i = state.markers.filter(f => f.store.number==issue.store.number)
        issue.latlng = latLng(issue.store.latitude, issue.store.longitude)
        if (i.length == 0) { console.log("Adding Store:", issue.store.number); state.markers.push(issue) }
      })
    },
    ADD_NEW_STORES(state, stores){
      stores.forEach(store => {
        state.new_stores.push(store)
      })
    },
    SET_MARKERS_TO_SHOW(state, issues){
      state.show_markers = issues
    }
  },
  actions: {
    set_markers_to_show({state,commit}, value) {
      if (value === 'all') {
        commit(state.markers)
      }
    },
    get_new_stores({commit}){
      let url = "http://"+ window.location.hostname + ":8000/api/v1/locations/new_stores/"
      console.log("URL", url)
      axios.get(url).then(data => {
        console.log(data)
        data = data.data
        commit("ADD_NEW_STORES", data)
      })
    }
  }
});
