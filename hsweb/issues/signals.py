# python base
import json
from asgiref.sync import async_to_sync
from django.db.models.signals import post_save
from django.dispatch import receiver
#third party
import channels.layers
from channels.db import database_sync_to_async

from .models import CommIssues
from .api.serializers import CommIssuesSerializer

GROUP_NAME = "comm-issues"
@receiver(post_save, sender=CommIssues)
def new_comm_issue_notifier(sender, instance, **kwargs):
    serializer = CommIssuesSerializer(instance)
    channel_layer = channels.layers.get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        GROUP_NAME, {'type': 'new-issue', "data": serializer.data}
    )

