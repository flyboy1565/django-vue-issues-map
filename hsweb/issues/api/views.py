from django.apps import apps
from rest_framework import routers, serializers, viewsets

from issues.models import *

from .serializers import *

class CommIssuesViewSet(viewsets.ModelViewSet):
    queryset = CommIssues.objects.filter(active=True)
    serializer_class = CommIssuesSerializer

class OutageTrackerViewSet(viewsets.ModelViewSet):
    queryset = OutageTracker.objects.all()
    serializer_class = OutageTrackerSerializer

class AppConfigurationsViewSet(viewsets.ModelViewSet):
    queryset = AppConfigurations.objects.all()
    serializer_class = AppConfigurationsSerializer
