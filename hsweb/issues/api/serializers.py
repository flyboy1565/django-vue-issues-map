from rest_framework import routers, serializers, viewsets

from issues.models import *
from locations.api.serializers import StoreSerializer

class CommIssuesSerializer(serializers.ModelSerializer):
    store = StoreSerializer()
    issue_type = serializers.CharField(source="get_issue_display")
    
    class Meta:
        model = CommIssues
        # would restrict fields in full feature applications based on group requirements
        fields = '__all__'


class OutageTrackerSerializer(serializers.ModelSerializer):
    sites = CommIssuesSerializer(many=True)

    class Meta:
        model = OutageTracker
        fields = '__all__'

class AppConfigurationsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AppConfigurations
        fields = '__all__'