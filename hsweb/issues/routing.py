from django.urls import path
from .consumers import IssuesConsumer

print("loading routing.py")

websocket_urlpatterns = [
    path('ws/comm-issues/', IssuesConsumer)
]
