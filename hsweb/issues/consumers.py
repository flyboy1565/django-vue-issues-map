# python base
import json
from asgiref.sync import async_to_sync
from django.db.models.signals import post_save
from django.dispatch import receiver
#third party
import channels.layers
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async

from .models import CommIssues
from .api.serializers import CommIssuesSerializer

GROUP_NAME = "comm-issues"

class IssuesConsumer(AsyncJsonWebsocketConsumer):
    
    async def connect(self):
        self.group_name = GROUP_NAME
        issues = CommIssues.objects.filter(active=True)
        serializer = CommIssuesSerializer(issues, many=True)
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        await self.accept()
        await self.send(json.dumps({'type': self.group_name, "message": serializer.data}))

    async def disconnect(self, close_code):
        self.channel_layer.group_discard(self.channel_name, self.group_name)

    async def chat_message(self, event):
        await self.send(json.dumps({'type': event['type'], "data": event['data']}))

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        channel_layer = channels.layers.get_channel_layer()
        events = {'type': 'new_message','text': message}
        await async_to_sync(channel_layer.group_send)(
            self.group_name, events
        )
