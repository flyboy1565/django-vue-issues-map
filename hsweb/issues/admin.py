from django.contrib import admin

from .models import *


class CommIssuesAdmin(admin.ModelAdmin):
    list_display = ('store', 'issue', 'ticket', 'confirmed', 'last_checked', 'ap3_down_since', 'ap3_restored_at', 'slaNotify', 'active',)
    list_filter = ('active', 'confirmed', 'slaNotify')

admin.site.register(CommIssues, CommIssuesAdmin)


admin.site.register(AppConfigurations)
admin.site.register(OutageTracker)