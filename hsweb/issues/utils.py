
def IssueChoices():
    return (
        ("comm/power", "Communications/Power Outage"),
        ("comm", "Communications"),
        ("linux", "Linux Down"),
        ("power", "Power Outage"),
        ('unknown', "Unknown"),
    )