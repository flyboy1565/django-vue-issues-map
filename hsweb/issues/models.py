from django.db import models

from .utils import IssueChoices


class CommIssues(models.Model):
    store = models.ForeignKey("locations.Store", on_delete=models.CASCADE)
    issue = models.CharField(choices=IssueChoices(), max_length=15)
    ticket = models.CharField(max_length=20, blank=True, null=True)
    confirmed = models.BooleanField(default=False)
    last_checked = models.DateTimeField(auto_now=True)
    ap3_down_since = models.DateTimeField(blank=True, null=True)
    ap3_restored_at = models.DateTimeField(blank=True, null=True)
    slaNotify = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return "CommIssue - {}".format(self.store.number)

    class Meta:
        verbose_name_plural = "Comm Issues"


class AppConfigurations(models.Model):
    name = models.CharField(max_length=50, unique=True)
    value = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return "AppConfig - {}".format(self.name)

    class Meta:
        verbose_name_plural = "App Configs"


class OutageTracker(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True, blank=True)
    sites = models.ManyToManyField(CommIssues, verbose_name="outage_issues")
    global_ticket_id = models.CharField(max_length=20, null=True, blank=True)
    providers = models.CharField(max_length=20,null=True, blank=True)
    states = models.CharField(max_length=20,null=True, blank=True)
    acknowledged = models.BooleanField(default=False)
    acknowledged_at = models.DateTimeField(null=True, blank=True)
    outage_interval = models.PositiveIntegerField(null=True, blank=True)
    active = models.BooleanField(default=True)

    def name(self):
        name = self.start_time.strftime('%Y-%m-%d')
        name += " - {} outage".format(self.sites.count())
        return name

    def __str__(self):
        return self.name()
    