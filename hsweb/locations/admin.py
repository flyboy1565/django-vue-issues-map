from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from django_admin_listfilter_dropdown.filters import (
    DropdownFilter, ChoiceDropdownFilter, RelatedDropdownFilter
)

from .models import *


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ['number', 'city', 'state', 'zip_code', 'timezone', 'get_district', 'get_region', 'get_division']    
    list_filter = (
        ('state', ChoiceDropdownFilter),
        ('timezone', DropdownFilter),
        ('district', RelatedDropdownFilter),
        ("acquisition", RelatedDropdownFilter),
    )

    def get_district(self, obj):
        if obj.district is None:
            return None
        return obj.district.number

    get_district.short_description = 'District'        

    def get_region(self, obj):
        if obj.district is None or obj.district.region is None:
            return None
        return obj.district.region.number

    get_region.short_description = 'Region'
    
    def get_division(self, obj):
        if obj.district is None or obj.district.region is None or obj.district.region.divisiono is None:
            return None
        return obj.district.region.division.number

    get_division.short_description = 'Division'



@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = [
        'number', 'address', 'city', 
        'state', 'zip_code', 'connect_to_store', 
        'region', 'get_store_count'
    ]    
    list_filter = (
        ('state', ChoiceDropdownFilter),
    )

    def get_store_count(self, obj):
        return obj.store_set.select_related().count()

    get_store_count.short_description = '# Of Stores' 


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = [
        'number', 'address', 'city', 
        'state', 'zip_code', 'connect_to_store', 
        'division', 
        'get_district_count',
        'get_store_count'
    ]    
    list_filter = (
        ('state', ChoiceDropdownFilter),
    )

    def get_district_count(self, obj):
        return obj.district_set.select_related().count()
    
    def get_store_count(self, obj):
        districts = obj.district_set.select_related()
        count = 0
        for district in districts:
            count += district.store_set.select_related().count()
        return count

    get_district_count.short_description = '# Of Districts' 
    get_store_count.short_description = '# Of Stores' 


@admin.register(Acquisition)
class AcquisitionAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'get_stores_count']

    def get_stores_count(self, obj):
        count = obj.store_set.select_related().count()
        # count = 0
        # locations/store/?acquisition__id__exact=1
        url = reverse("admin:app_list", kwargs={'app_label': 'locations'}) + 'store/?aquisition={}'.format(obj.pk)
        link = '''<a href="{}">{}</a>'''.format(url, count)
        print(link)
        return mark_safe(link)

    get_stores_count.short_description = "# Of Stores"

admin.site.register(StorePhoneNumber)
admin.site.register(Division)
admin.site.register(DistributionCenter)
# admin.site.register(Acquisition)
admin.site.register(StoreType)