from django.db import models
from django.utils import timezone

from localflavor.us.models import USStateField, USZipCodeField
from phonenumber_field.modelfields import PhoneNumberField

from .utils import StatusChoices

from issues.models import CommIssues


class Store(models.Model):
    number = models.PositiveIntegerField(unique=True)
    # location
    longitude = models.DecimalField(max_digits=11, decimal_places=8)
    latitude = models.DecimalField(max_digits=11, decimal_places=8)
    address = models.CharField(max_length=60)
    city = models.CharField(max_length=60)
    state = USStateField(null=True, blank=True)
    zip_code = USZipCodeField()
    district = models.ForeignKey("District", on_delete=models.SET_NULL, null=True, blank=True)
    dc = models.ForeignKey('DistributionCenter', on_delete=models.SET_NULL, null=True, blank=True)
    priority = models.PositiveIntegerField(null=True, blank=True)
    hub = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True)
    is_hub = models.BooleanField(default=False)
    previous_year_sales = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    open_date = models.DateField(null=True, blank=True)
    close_date = models.DateField(null=True, blank=True)
    weekday_open = models.TimeField(null=True, blank=True)
    weekday_close = models.TimeField(null=True, blank=True)
    sat_open = models.TimeField(null=True, blank=True)
    sat_close = models.TimeField(null=True, blank=True)
    sun_open = models.TimeField(null=True, blank=True)
    sun_close = models.TimeField(null=True, blank=True)
    timezone = models.CharField(max_length=4, null=True, blank=True)
    full_timezone = models.CharField(max_length=50, null=True, blank=True)
    acquisition = models.ForeignKey("Acquisition", on_delete=models.SET_NULL, null=True, blank=True)
    deactivated_date = models.DateField(null=True, blank=True)
    status = models.CharField(choices=StatusChoices(), max_length=5)
    store_type = models.ForeignKey("StoreType", on_delete=models.SET_NULL, null=True, blank=True)

    def open_issues(self):
        issues = CommIssues.objects.filter(store=self, active=True)
        if issues.count() == 0:
            return []
        return issues

    @property
    def days_until_open(self):
        now = timezone.now()
        if self.open_date < now.date():
            return 0
        else:
            return (self.open_date - now.date()).days
    
    @property
    def full_address(self):
        return f'''{self.address}\n {self.city}, {self.state} {self.zip_code}'''

    def __str__(self):
        return "Store - {}".format(self.number)
    
    class Meta:
        ordering = ('number',)


class StorePhoneNumber(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    line_number = models.PositiveSmallIntegerField()
    phone_number = PhoneNumberField(null=False, blank=False, unique=True)
    tag = models.CharField(max_length=10)
    last_updated = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return "{}".format(self.phone_number)


class District(models.Model):
    number = models.PositiveIntegerField(unique=True)
    address = models.CharField(max_length=60)
    city = models.CharField(max_length=60)
    state = USStateField(null=True, blank=True)
    zip_code = USZipCodeField(null=True, blank=True)
    connect_to_store = models.ForeignKey(Store, on_delete=models.SET_NULL, blank=True, null=True, related_name="district_location")
    region = models.ForeignKey('Region', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return "District - {}".format(self.number)


class Region(models.Model):
    number = models.PositiveIntegerField(unique=True)
    address = models.CharField(max_length=60)
    city = models.CharField(max_length=60)
    state = USStateField(null=True, blank=True)
    zip_code = USZipCodeField(null=True, blank=True)
    connect_to_store = models.ForeignKey(Store, on_delete=models.SET_NULL, blank=True, null=True, related_name="region_location")
    division = models.ForeignKey('Division', on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return "Region - {}".format(self.number)


class Division(models.Model):
    number = models.PositiveIntegerField(unique=True)
    address = models.CharField(max_length=60)
    city = models.CharField(max_length=60)
    state = USStateField(null=True, blank=True)
    zip_code = USZipCodeField(null=True, blank=True)

    def __str__(self):
        return "Division - {}".format(self.number)


class DistributionCenter(models.Model):
    number = models.PositiveIntegerField(unique=True)
    address = models.CharField(max_length=60)
    city = models.CharField(max_length=60)
    state = USStateField(null=True, blank=True)
    zip_code = USZipCodeField(null=True, blank=True)
    city_counter = models.ForeignKey(Store, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return "DC - {}".format(self.number)


class Acquisition(models.Model):
    company_name = models.CharField(max_length=50)

    def __str__(self):
        return self.company_name


class StoreType(models.Model):
    type = models.CharField(max_length=50)

    def __str__(self):
        return self.type