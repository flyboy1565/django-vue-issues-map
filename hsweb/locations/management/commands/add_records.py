import csv, time
from django.core.management.base import BaseCommand
from locations.models import *

class Command(BaseCommand):
    help = "Create database using csv."

    def add_arguments(self, parser):
        parser.add_argument('filepath')

    def handle(self, *args, **options):
        print('opening file path:', options['filepath'])
        districts = []
        dcs = []
        infile = csv.DictReader(open(options['filepath']))
        for line, row in enumerate(infile):
            if int(row['store']) > 2942:
                store = Store()
                store.number = int(row['store'])
                print('Store:', row['store'])
                store.longitude = row['longitude']
                store.latitude = row['latitude']
                store.address = row['address']
                store.city = row['city']
                store.state = row['state']
                store.zip_code = row['zip_code']
                print('District:', row['district'])
                if row['district'] not in districts:
                    d = District.objects.get_or_create(number=row['district'])
                    if d[1]:
                        districts.append(row['district'])
                    d = d[0]
                print('District:', d)
                store.district = d
                print('DC:', row['dc'])
                if row['dc'] not in dcs:
                    dc = DistributionCenter.objects.get_or_create(number=row['dc'])
                    if dc[1]:
                        dcs.append(row['dc'])
                    dc = dc[0]
                store.dc = dc
                print('DC:', store.dc)
                print('HUB:', row['hub'])
                if row['hub'] != '':
                    hub = Store.objects.filter(number=row['hub'])
                    if hub.count() > 0:
                        store.hub = hub[0]
                if row['is_hub'] == '0':
                    store.is_hub = False
                else:
                    store.is_hub = True
                store.open_date = row['open_date']
                store.close_date = row['close_date']
                if row['close_date'] == '0001-01-01' or row['close_date'] == '':
                    store.close_date = None
                if row['open_date'] == '0001-01-01' or row['open_date'] == '':
                    store.open_date = None
                store.weekday_open = row['weekday_open']
                store.weekday_close = row['weekday_close']
                store.sat_open = row['sat_open']
                store.sat_close = row['sat_close']
                store.sun_open = row['sun_open']
                store.sun_close = row['sun_close']
                store.timezone = row['timezone']
                store.full_timezone = row['full_timezone']
                if row['acquisition'] != '':
                    a = Acquisition.objects.get_or_create(company_name=row['acquisition'])[0]
                elif row['bond'] != '0':
                    a = Acquisition.objects.get_or_create(company_name='Bond')[0]
                    store.acquisition = a
                if row['active'] != '0':
                    store.status = 'O'
                else:
                    store.status = 'C'
                if store.is_hub:
                    store.store_type = StoreType.objects.get_or_create(type="Hub Store")[0]
                else:
                    store.store_type = StoreType.objects.get_or_create(type="Store")[0]
                store.save()
                print("Completed: ", row['store'])
                # time.sleep(1)