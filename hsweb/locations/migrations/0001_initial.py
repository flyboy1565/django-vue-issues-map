# Generated by Django 2.2.4 on 2019-08-20 02:41

from django.db import migrations, models
import django.db.models.deletion
import localflavor.us.models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Acquisition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='DistributionCenter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(unique=True)),
                ('address', models.CharField(max_length=60)),
                ('city', models.CharField(max_length=60)),
                ('state', localflavor.us.models.USStateField(blank=True, max_length=2, null=True)),
                ('zip_code', localflavor.us.models.USZipCodeField(blank=True, max_length=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(unique=True)),
                ('address', models.CharField(max_length=60)),
                ('city', models.CharField(max_length=60)),
                ('state', localflavor.us.models.USStateField(blank=True, max_length=2, null=True)),
                ('zip_code', localflavor.us.models.USZipCodeField(blank=True, max_length=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Division',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(unique=True)),
                ('address', models.CharField(max_length=60)),
                ('city', models.CharField(max_length=60)),
                ('state', localflavor.us.models.USStateField(blank=True, max_length=2, null=True)),
                ('zip_code', localflavor.us.models.USZipCodeField(blank=True, max_length=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(unique=True)),
                ('longitude', models.DecimalField(decimal_places=8, max_digits=11)),
                ('latitude', models.DecimalField(decimal_places=8, max_digits=11)),
                ('address', models.CharField(max_length=60)),
                ('city', models.CharField(max_length=60)),
                ('state', localflavor.us.models.USStateField(blank=True, max_length=2, null=True)),
                ('zip_code', localflavor.us.models.USZipCodeField(max_length=10)),
                ('priority', models.PositiveSmallIntegerField()),
                ('is_hub', models.BooleanField(default=False)),
                ('previous_year_sales', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('open_date', models.DateField(blank=True, null=True)),
                ('close_date', models.DateField(blank=True, null=True)),
                ('weekday_open', models.TimeField(blank=True, null=True)),
                ('weekday_close', models.TimeField(blank=True, null=True)),
                ('sat_open', models.TimeField(blank=True, null=True)),
                ('sat_close', models.TimeField(blank=True, null=True)),
                ('sun_open', models.TimeField(blank=True, null=True)),
                ('sun_close', models.TimeField(blank=True, null=True)),
                ('timezone', models.CharField(blank=True, max_length=4, null=True)),
                ('full_timezone', models.CharField(blank=True, max_length=50, null=True)),
                ('deactivated_date', models.DateField(blank=True, null=True)),
                ('status', models.CharField(choices=[('O', 'Open'), ('C', 'Closed'), ('TC', 'Temp Closed'), ('U', 'Unknown')], max_length=5)),
                ('acquisition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Acquisition')),
                ('dc', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.DistributionCenter')),
                ('district', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.District')),
                ('hub', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Store')),
            ],
        ),
        migrations.CreateModel(
            name='StoreType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='StorePhoneNumber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('line_number', models.PositiveSmallIntegerField()),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(max_length=128, unique=True)),
                ('tag', models.CharField(max_length=10)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='locations.Store')),
            ],
        ),
        migrations.AddField(
            model_name='store',
            name='store_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.StoreType'),
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(unique=True)),
                ('address', models.CharField(max_length=60)),
                ('city', models.CharField(max_length=60)),
                ('state', localflavor.us.models.USStateField(blank=True, max_length=2, null=True)),
                ('zip_code', localflavor.us.models.USZipCodeField(blank=True, max_length=10, null=True)),
                ('connect_to_store', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='region_location', to='locations.Store')),
                ('division', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Division')),
            ],
        ),
        migrations.AddField(
            model_name='district',
            name='connect_to_store',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='district_location', to='locations.Store'),
        ),
        migrations.AddField(
            model_name='district',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Region'),
        ),
        migrations.AddField(
            model_name='distributioncenter',
            name='city_counter',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Store'),
        ),
    ]
