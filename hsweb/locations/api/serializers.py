from rest_framework import routers, serializers, viewsets

from locations.models import Store, District, DistributionCenter, Acquisition, StoreType
from issues.models import CommIssues

class CurrentIssueSerializer(serializers.ModelSerializer):

    class Meta:
        model = CommIssues
        fields = "__all__"

class NewStoreSerializer(serializers.ModelSerializer):
    full_address = serializers.CharField()
    status = serializers.CharField(source="get_status_display")
    days_till_open = serializers.CharField(source="days_until_open")

    class Meta:
        model = Store
        fields = '__all__'

class DistrictSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = District
        fields = '__all__'

class DistributionCenterSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = DistributionCenter
        fields = '__all__'

class AcquisitionSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Acquisition
        fields = '__all__'


class StoreTypeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = StoreType
        fields = '__all__'


class StoreSerializer(serializers.ModelSerializer):
    full_address = serializers.CharField()
    status = serializers.CharField(source="get_status_display")
    issues = CurrentIssueSerializer(source="open_issues", many=True)
    district = DistrictSerializer()
    
    class Meta:
        model = Store
        fields = '__all__'