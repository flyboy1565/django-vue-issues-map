from django.utils import timezone
from django.apps import apps
from rest_framework import routers, serializers, viewsets

from locations.models import Store, District, DistributionCenter, Acquisition, StoreType

from .serializers import *

class StoreViewSet(viewsets.ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    lookup_field = "number"

class NewStoreViewSet(viewsets.ModelViewSet):
    queryset = Store.objects.filter(open_date__gt=timezone.now())
    serializer_class = NewStoreSerializer
    lookup_field = "number"

class DistrictViewSet(viewsets.ModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    lookup_field = "number"

class DistributionCenterViewSet(viewsets.ModelViewSet):
    queryset = DistributionCenter.objects.all()
    serializer_class = DistributionCenterSerializer
    lookup_field = "number"

class AcquisitionViewSet(viewsets.ModelViewSet):
    queryset = Acquisition.objects.all()
    serializer_class = AcquisitionSerializer

class StoreTypeViewSet(viewsets.ModelViewSet):
    queryset = StoreType.objects.all()
    serializer_class = StoreTypeSerializer
    lookup_field = "number"