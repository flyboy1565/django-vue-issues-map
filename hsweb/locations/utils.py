
def StatusChoices():
    return (
        ("O", "Open"),
        ("C", "Closed"),
        ("TC", "Temp Closed"),
        ("U", "Unknown"),
    )