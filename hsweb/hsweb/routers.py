from rest_framework import routers, serializers, viewsets
from rest_framework.urlpatterns import format_suffix_patterns

from locations.api import views as location_views
from issues.api import views as issue_views

router = routers.DefaultRouter()

# Locations Views
router.register('v1/locations/stores', location_views.StoreViewSet, 'stores')
router.register('v1/locations/new_stores', location_views.NewStoreViewSet, 'new_stores')
router.register('v1/locations/district', location_views.DistrictViewSet)
router.register('v1/locations/dc', location_views.DistributionCenterViewSet)
router.register('v1/locations/acquisition', location_views.AcquisitionViewSet)
router.register('v1/locations/storetype', location_views.StoreTypeViewSet)

# Issue Views
router.register('v1/issues/comms', issue_views.CommIssuesViewSet)
router.register('v1/issues/outages', issue_views.OutageTrackerViewSet)

